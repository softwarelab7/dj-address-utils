============
Installation
============

At the command line::

    $ easy_install dj-address-utils

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj-address-utils
    $ pip install dj-address-utils
