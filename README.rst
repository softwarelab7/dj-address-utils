=============================
Django Address Utils
=============================

.. image:: https://badge.fury.io/py/dj-address-utils.png
    :target: https://badge.fury.io/py/dj-address-utils

.. image:: https://travis-ci.org/aidzz/dj-address-utils.png?branch=master
    :target: https://travis-ci.org/aidzz/dj-address-utils

Your project description goes here

Documentation
-------------

The full documentation is at https://dj-address-utils.readthedocs.org.

Quickstart
----------

Install Django Address Utils::

    pip install dj-address-utils

Then use it in a project::

    import dj_address_utils

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
