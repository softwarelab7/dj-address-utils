from decimal import Decimal

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from django_extensions.db.fields import UUIDField

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class UserCoverage(models.Model):
    user = models.OneToOneField(AUTH_USER_MODEL, related_name='coverage')
    distance = models.DecimalField(max_digits=8, decimal_places=2)
    KM = "km"
    MI = "mi"
    UNITS_CHOICES = (
        (MI, _("Miles")),
        (KM, _("Kilometers")),
    )
    units = models.CharField(choices=UNITS_CHOICES, default=UNITS_CHOICES[0][0],
                             max_length=20, verbose_name=_("Units"))
    uuid = UUIDField(unique=True)

    def __unicode__(self):
        return _(u"{} - {} {}".format(self.user.get_full_name(),
                 self.distance, self.units))

    @property
    def distance_in_km(self):
        if self.units == self.MI:
            return self.distance / Decimal(0.62137)
        elif self.units == self.KM:
            return self.distance

    @property
    def distance_in_mi(self):
        if self.units == self.MI:
            return self.distance
        elif self.units == self.KM:
            return self.distance * Decimal(0.62137)
