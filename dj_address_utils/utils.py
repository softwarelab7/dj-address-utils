import logging
import pytz

from decimal import Decimal as D
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from geopy import geocoders
from geopy.exc import GeopyError
from math import radians, sin, cos, sqrt, asin
from pygeocoder import Geocoder
from requests.exceptions import ConnectionError, Timeout
from socket import error as SocketError

logger = logging.getLogger(__name__)


def _convert_km_to_mi(km):
    return D(km) * D(0.62137)


def haversine(lat1, lon1, lat2, lon2):
    """
    returns great circle distances between 2 points from their longitudes
    and latitudes
    from http://rosettacode.org/wiki/Haversine_formula#Python
    """

    R = 6372.8  # Earth radius in kilometers

    lat1 = D(lat1)
    lon1 = D(lon1)
    lat2 = D(lat2)
    lon2 = D(lon2)

    dLat = radians(lat2 - lat1)
    dLon = radians(lon2 - lon1)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    a = sin(dLat / 2)**2 + cos(lat1) * cos(lat2) * sin(dLon / 2)**2
    c = 2 * asin(sqrt(a))

    return R * c


def geocode_address(address_text, postcode=None):
    """
    get geocoded address and checks address validity
    """
    try:
        logger.info(u'PYGEOCODER: validating address {}'.format(address_text))
        result = Geocoder(api_key=settings.GOOGLE_MAPS_API_KEY).geocode(address_text)
    except (ConnectionError, Timeout, SocketError, Exception) as e:
        logger.warn('Unable to validate address due to: {}'.format(e))
        raise e
    if result.valid_address or 'subpremise' in result.current_data['types'] or 'premise' in result.current_data['types']:
        if postcode and postcode != result.postal_code:
            raise ValueError(_("That is not a valid address for that postal code."))
        street = ""
        if result.route:
            if result.street_number:
                street = u"{} {}".format(result.street_number, result.route)
            else:
                street = result.route
            if result.subpremise:
                street = u"{} #{}".format(street, result.subpremise)

        return (street, result)
    else:
        logger.error(u"Address {} is not valid. Returned types {} and "
            "formatted address is {}".format(address_text,
                result.current_data['types'], result.current_data['formatted_address']))
        raise ValueError(_("That is not a valid address."))


def get_address_lat_long(address):
    """
    Gets latitude and longitude from address
    Uses pygeocode geocoder if latitude and longitude are None
    Returns valid, latitude, longitude
    """
    if address is None:
        return (False, None, None)
    if address.latitude is None or address.longitude is None:
        try:
            logger.info(u'PYGEOCODER: getting latitude and longitude for {}'.format(address.input_address_text))
            result = Geocoder(api_key=settings.GOOGLE_MAPS_API_KEY).geocode(address.input_address_text)
        except Exception as e:
            logger.info(u'Error getting latitude and longitude: {}'.format(e))
            return (False, None, None)
        if result.valid_address or 'subpremise' in result.current_data['types'] or 'premise' in result.current_data['types']:
            address.latitude = result.latitude
            address.longitude = result.longitude
            address.save()
        else:
            return (False, None, None)
    return (True, address.latitude, address.longitude)


def get_address_timezone(address):
    """
    Gets timezone from address
    Gets timezone from address using geopy geocoder if address timezone
    is None
    Returns None if address is not valid
    """
    if not address:
        return None
    if address.timezone is None:
        if settings.MOCK_GEOCODE_TIMEZONE:
            logger.info(_(u"Mocking get_geocoded timezone, returning {}".format(
                settings.MOCK_TIMEZONE)))
            address.timezone = pytz.timezone(settings.MOCK_TIMEZONE)
        else:
            address.timezone = get_geocoded_timezone(address)
        address.save()
    return address.timezone


def get_default_address_timezone(default_address):
    """
    Gets timezone from UserDefaultAddress
    Gets timezone from address in UserDefaultAddress if timezone is None
    """
    if not default_address:
        return None
    if default_address.timezone is None and default_address.address:
        default_address.timezone = get_address_timezone(default_address.address)
    default_address.save()
    return default_address.timezone


def get_geocoded_timezone(address):
    if address is None:
        return None
    if address.latitude is None or address.longitude is None:
        (valid, latitude, longitude) = get_address_lat_long(address)
        if not valid:
            return None
    else:
        latitude = address.latitude
        longitude = address.longitude
    logger.info(u'GEOPY: getting timezone for {}'.format(address.input_address_text))
    return get_timezone_from_lat_long(latitude, longitude)


def get_zipcode_timezone(zipcode):
    if not zipcode.timezone:
        zipcode.timezone = get_timezone_from_lat_long(zipcode.latitude, zipcode.longitude)
        zipcode.save()
    return zipcode.timezone


def get_timezone_from_lat_long(latitude, longitude):
    try:
        google_geocode = geocoders.GoogleV3(api_key=settings.GOOGLE_MAPS_API_KEY)
        lat_long = (latitude, longitude)
        return google_geocode.timezone(lat_long)
    except GeopyError:
        return None


def is_address_in_coverage(address, center, coverage, zipcode=None):
    """
    address is address to check
    center is center of coverage
    """
    coverage_in_km = coverage.distance_in_km
    (valid, center_latitude, center_longitude) = get_address_lat_long(center)
    coverage_dict = {'in_coverage': False, 'error': None, 'coverage_mi': None, 'distance_mi': None}
    if not valid:
        coverage_dict['error'] = 'center'
        return coverage_dict
    if address:
        (valid, latitude, longitude) = get_address_lat_long(address)
        if not valid:
            coverage_dict['error'] = 'address'
            return coverage_dict
    elif zipcode:
        latitude = zipcode.latitude
        longitude = zipcode.longitude
    else:
        coverage_dict['error'] = 'zipcode'
        return coverage_dict

    distance = haversine(center_latitude, center_longitude, latitude, longitude)
    if distance > coverage_in_km:
        coverage_mi = _convert_km_to_mi(coverage_in_km)
        distance_mi = _convert_km_to_mi(distance)
        coverage_dict['error'] = 'coverage'
        coverage_dict['coverage_mi'] = coverage_mi
        coverage_dict['distance_mi'] = distance_mi
    else:
        coverage_dict['in_coverage'] = True
    return coverage_dict
