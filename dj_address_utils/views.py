# -*- coding: utf-8 -*-
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    UpdateView,
    ListView
)

from .models import (
	Coverage,
)


class CoverageCreateView(CreateView):

    model = Coverage


class CoverageDeleteView(DeleteView):

    model = Coverage


class CoverageDetailView(DetailView):

    model = Coverage


class CoverageUpdateView(UpdateView):

    model = Coverage


class CoverageListView(ListView):

    model = Coverage

