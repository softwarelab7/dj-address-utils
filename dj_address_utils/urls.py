# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(
        regex="^Coverage/~create/$",
        view=views.CoverageCreateView.as_view(),
        name='Coverage_create',
    ),
    url(
        regex="^Coverage/(?P<pk>\d+)/~delete/$",
        view=views.CoverageDeleteView.as_view(),
        name='Coverage_delete',
    ),
    url(
        regex="^Coverage/(?P<pk>\d+)/$",
        view=views.CoverageDetailView.as_view(),
        name='Coverage_detail',
    ),
    url(
        regex="^Coverage/(?P<pk>\d+)/~update/$",
        view=views.CoverageUpdateView.as_view(),
        name='Coverage_update',
    ),
    url(
        regex="^Coverage/$",
        view=views.CoverageListView.as_view(),
        name='Coverage_list',
    ),
	]
